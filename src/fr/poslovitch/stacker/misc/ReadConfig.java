package fr.poslovitch.stacker.misc;

import java.io.File;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ReadConfig {

	File configFile = new File("plugins/Stacker", "config.yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(this.configFile);

	public boolean defaultOn(){return this.config.getBoolean("defaultOn");}

	public boolean showMessages(){return this.config.getBoolean("showMessages");}

	public int throwStrength(){return this.config.getInt("throwStrength");}

	public List<String> worlds(){return this.config.getStringList("disableOnWorlds");}
	
	public String prefix(){return this.config.getString("prefix");}
}
