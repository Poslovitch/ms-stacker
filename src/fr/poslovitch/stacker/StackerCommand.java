package fr.poslovitch.stacker;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.poslovitch.stacker.misc.ReadConfig;

public class StackerCommand implements CommandExecutor{

	Stacker main;
	ReadConfig rc = new ReadConfig();

	public StackerCommand(Stacker plugin){
		main = plugin;
		main.getCommand("stacker").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(args.length == 0) p.sendMessage(rc.prefix() + ChatColor.WHITE + " Mauvaise syntaxe, entrez " + ChatColor.GOLD + "/stacker help");
			else if(args.length == 1){
				if(args[0].equalsIgnoreCase("on")){
					if(p.hasPermission("stacker.on")){
						if(!StackerEvents.players.contains(p)){
							p.sendMessage(rc.prefix() + ChatColor.GOLD + " Stackmode " + ChatColor.GREEN + "activ�");
							StackerEvents.addPlayer(p);
						}

						else p.sendMessage(rc.prefix() + ChatColor.RED + " Stackmode d�j� activ�!");
					}
					else p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire �a!");
				}

				else if(args[0].equalsIgnoreCase("off")){
					if(p.hasPermission("stacker.off")){
						if(StackerEvents.players.contains(p)){
							p.sendMessage(rc.prefix() + ChatColor.GOLD + " Stackmode " + ChatColor.RED + "d�sactiv�");
							StackerEvents.removePlayer(p);
						}

						else p.sendMessage(rc.prefix() + ChatColor.RED + " Stackmode d�j� d�sactiv�!");
					}
					else p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire �a!");
				}

				else if (args[0].equalsIgnoreCase("help")){
					if (p.hasPermission("stacker.help")){
						p.sendMessage(ChatColor.GRAY + "--+--+--+--" + rc.prefix() + ChatColor.GRAY + "--+--+--+--");
						if (p.hasPermission("stacker.on")) p.sendMessage(ChatColor.WHITE + "/stacker on" + ChatColor.GRAY + ": Active le stackmode!");
						if (p.hasPermission("stacker.off")) p.sendMessage(ChatColor.WHITE + "/stacker off" + ChatColor.GRAY + ": D�sactive le stackmode!");
						if (p.hasPermission("stacker.help")) p.sendMessage(ChatColor.WHITE + "/stacker help" + ChatColor.GRAY + ": Affiche l'aide pour Stacker!");
						p.sendMessage(ChatColor.GRAY + "--+--+--+--+--+--+--+--+--+--+--+--+--");
					}
					else p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire �a!");
				}

				else p.sendMessage(rc.prefix() + ChatColor.WHITE + " Mauvaise syntaxe, entrez " + ChatColor.GOLD + "/stacker help");
			}

			else if(args.length == 2){
				if (args[0].equalsIgnoreCase("on")){
					if (p.hasPermission("stacker.turnonother")){
						Player pl = Bukkit.getPlayer(args[1]);
						if (pl != null){
							StackerEvents.addPlayer(pl);
							pl.sendMessage(rc.prefix() + ChatColor.GOLD + " Stackmode " + ChatColor.GREEN + "activ�");
							p.sendMessage(rc.prefix() + ChatColor.GREEN + " Stackmode activ� pour " + ChatColor.GOLD + pl.getName() + ChatColor.GREEN + " !");
						}
						else p.sendMessage(rc.prefix() + ChatColor.RED + " Ce joueur est introuvable.");
					}
					else p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire �a!");
				}

				else if (args[0].equalsIgnoreCase("off")){
					if (p.hasPermission("stacker.turnoffother")){
						Player pl = Bukkit.getPlayer(args[1]);
						if (pl != null){
							StackerEvents.addPlayer(pl);
							pl.sendMessage(rc.prefix() + ChatColor.GOLD + " Stackmode " + ChatColor.RED + "d�sactiv�");
							p.sendMessage(rc.prefix() + ChatColor.GREEN + " Stackmode d�sactiv� pour " + ChatColor.GOLD + pl.getName() + ChatColor.GREEN + " !");
						}
						else p.sendMessage(rc.prefix() + ChatColor.RED + " Ce joueur est introuvable.");
					}
					else p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire �a!");
				}

				else p.sendMessage(rc.prefix() + ChatColor.WHITE + " Mauvaise syntaxe, entrez " + ChatColor.GOLD + "/stacker help");
			}
			else p.sendMessage(rc.prefix() + ChatColor.WHITE + " Mauvaise syntaxe, entrez " + ChatColor.GOLD + "/stacker help");
		}
		else sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
		return true;
	}
}
