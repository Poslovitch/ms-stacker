package fr.poslovitch.stacker;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.poslovitch.stacker.misc.ReadConfig;
import fr.poslovitch.stacker.misc.Vecteur;
import net.md_5.bungee.api.ChatColor;

public class StackerEvents implements Listener{

	Stacker main;

	public StackerEvents(Stacker plugin){
		this.main = plugin;
		this.main.getServer().getPluginManager().registerEvents(this, this.main);
	}

	static ArrayList<Player> players = new ArrayList<Player>();
	Vecteur vector = new Vecteur();
	ReadConfig rc = new ReadConfig();

	@EventHandler
	public void onEntityInteract(PlayerInteractEntityEvent e){
		Player p = e.getPlayer();
		if(e.getRightClicked() instanceof Player){
			if(p.hasPermission("stacker.stack")){
				Player clicked = (Player) e.getRightClicked();
				if(!clicked.hasPermission("stacker.notstackable")){
					if(players.contains(p)){
						if(players.contains(clicked)){
							if(!rc.worlds().contains(p.getWorld().getName())) p.setPassenger(clicked);
							else if(rc.showMessages()) p.sendMessage(rc.prefix() + ChatColor.RED + " Stacker n'est pas activ� dans ce monde!");
						}
						else if(rc.showMessages()) p.sendMessage(rc.prefix() + ChatColor.GREEN + clicked.getName() + ChatColor.RED + " ne peut pas �tre stack� !");
					}
				}
				else if(rc.showMessages()) p.sendMessage(rc.prefix() + ChatColor.GREEN + clicked.getName() + ChatColor.RED + " n'est pas stackable !");
			}
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(e.getAction() == Action.LEFT_CLICK_AIR){
			if((p.getPassenger() instanceof Player)){
				if (p.hasPermission("stacker.throw")){
					Player pass = (Player)p.getPassenger();
					if (!pass.hasPermission("stacker.notthrowable")){
						pass.leaveVehicle();
						Location loc = p.getLocation();
						int strength = rc.throwStrength();
						if (strength > 0) pass.setVelocity(vector.giveVector(loc).multiply(strength));
						else if (strength < -1) pass.teleport(loc);
					}
					else if (rc.showMessages()) p.sendMessage(rc.prefix() + ChatColor.GREEN + p.getName() + ChatColor.RED + " ne peut �tre lanc� !");
				}
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		boolean defaultOn = rc.defaultOn();
		if(defaultOn) addPlayer(e.getPlayer());
	}

	public static void addPlayer(Player p){
		if(!players.contains(p)) players.add(p);
	}

	public static void removePlayer(Player p){
		if(players.contains(p)) players.remove(p);
	}
}
