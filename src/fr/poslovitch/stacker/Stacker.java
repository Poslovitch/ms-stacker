package fr.poslovitch.stacker;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Stacker extends JavaPlugin{

	File configFile = new File("plugins/Stacker", "config.yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(this.configFile);

	@Override
	public void onEnable(){
		if (new File("plugins/Stacker/config.yml").exists()){
			this.config = getConfig();
			this.config.options().copyDefaults(true);
			getLogger().info("Loaded configuration.");
		}
		
		else{
			saveDefaultConfig();
			this.config = getConfig();
			this.config.options().copyDefaults(true);
			getLogger().info("Created and loaded new configuration file.");
		}
		
		new StackerEvents(this);
		new StackerCommand(this);
		
		getLogger().info("Stacker successfully loaded.");
	}

	@Override
	public void onDisable(){
		getLogger().info("Stacker successfully disabled.");
	}
}
